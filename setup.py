# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

import os
import sys

from setuptools import find_packages, setup
from setuptools.command.test import test as TestCommand


# Allow setup.py to be run from any where. This allows things like the README
# to be found as well as files when running Tox tests.
os.chdir(os.path.dirname(os.path.abspath(__file__)))


with open('README.rst') as readme:
    README = readme.read()


# From http://tox.readthedocs.org/en/latest/example/basic.html#integration-with-setuptools-distribute-test-commands
class Tox(TestCommand):
    """ A setuptools TestCommand that allows tests to be run with tox """

    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        # Import here, because outside the eggs aren't loaded
        import tox
        errno = tox.cmdline(self.test_args)
        sys.exit(errno)

tests = [
    'tox>=1.7.0',
    'mock==1.0.1',
]

multimedia = [
    'audioread==2.1.1',
    'FFVideo==0.0.13',  # Requires some system libs. See https://pypi.python.org/pypi/FFVideo.
]

documents = [
    'PyPDF2==1.19',
    'xlrd==0.9.0',
    'epub==0.5.2',
    'pyth==0.5.6',
    'psd-tools==0.8.4',
    'docx==0.2.4',
    'python-pptx==0.6.2',
    'pdfminer==20131113',
]

software = [
    'polib==1.0.3',
    'pyaml==14.05.7',
]

xml = [
    'BeautifulSoup4==4.3.2',
    'UCFlib==0.2.1',
]

extras = multimedia + documents + software + xml

setup(
    name = 'liox_file_analyzer',
    version = '1.0',
    description = 'Lionbridge Technologies file analyzing tool.',
    long_description = README,
    url = 'https://bitbucket.org/liox-ondemand/liox_file_analyzer_v1',
    license = 'MIT License',
    author = 'Lionbridge Technologies, Inc.',
    author_email = 'ondemand@lionbridge.com',
    packages = find_packages(),
    include_package_data = True,
    install_requires = [
        'Django>=1.4,<=2.0',  # Not an ideal requirement. Will remove that soon.
        'nltk==3.0.3',  # Requires catdoc lib in system.
        'bleach==1.2.2',
    ],
    extras_require = {
        'multimedia': multimedia,
        'documents': documents,
        'software': software,
        'xml': xml,
        'extras': extras,
    },
    tests_require = tests + extras,
    cmdclass = {
        'test': Tox,
    },
    classifiers = (
        'Development Status :: 5 - Production/Stable',
        'Framework :: Django :: 1.8',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2.7',
        'Topic :: Text Processing',
    ),
)
