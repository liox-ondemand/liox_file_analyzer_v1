import os

from unittest import TestCase

from .. import extractors
from ...exceptions import FileExtensionNotSupportedException


TEST_DIR = os.path.dirname(os.path.abspath(__file__))
TEST_FILES_DIR = os.path.join(TEST_DIR, 'test_files')


class ExtractPdfTests(TestCase):
    def test_simple_pdf(self):
        test_file = os.path.join(TEST_FILES_DIR, 'simple.pdf')
        text = extractors.extract_pdf_text(test_file)
        self.assertTrue(isinstance(text, basestring))
        # Checks if the result contains a word from the pdf
        self.assertRegexpMatches(text, 'Hello')

    def test_long_pdf(self):
        """
            Tests if the extractor works in longer files
        """
        test_file = os.path.join(TEST_FILES_DIR, '5_0_customer.pdf')
        text = extractors.extract_pdf_text(test_file)
        self.assertTrue(isinstance(text, basestring))
        # Checks if the result contains a word from the pdf
        self.assertRegexpMatches(text, 'HORLOGE')


class ExtractPptxTests(TestCase):
    def test_simple_pptx(self):
        test_file = os.path.join(TEST_FILES_DIR, 'simple.pptx')
        text = extractors.extract_pptx_text(test_file)
        self.assertTrue(isinstance(text, basestring))
        # Checks if the result contains a word from the pdf
        self.assertRegexpMatches(text, 'Title')

    def test_long_pptx(self):
        test_file = os.path.join(TEST_FILES_DIR, 'constant_programming.pptx')
        text = extractors.extract_pptx_text(test_file)
        self.assertTrue(isinstance(text, basestring))
        # Checks if the result contains a word from the pdf
        self.assertRegexpMatches(text, 'Programming')

    def test_include_notes(self):
        """ Verifies result if `include_notes` is True """
        test_file = os.path.join(TEST_FILES_DIR, 'simple_with_note.pptx')
        text = extractors.extract_pptx_text(test_file)
        self.assertTrue(isinstance(text, basestring))
        # Verify that the result containts the text from the Note
        self.assertRegexpMatches(text, 'This is a sample note')
        self.assertRegexpMatches(text, 'This is another note')

    def test_exclude_notes(self):
        """ Verifies result if `include_notes` is False """
        test_file = os.path.join(TEST_FILES_DIR, 'simple_with_note.pptx')
        text = extractors.extract_pptx_text(test_file, include_notes=False)
        self.assertTrue(isinstance(text, basestring))
        # Verify that the result does not contain the text from the Note
        self.assertNotRegexpMatches(text, 'This is a sample note')
        self.assertNotRegexpMatches(text, 'This is another note')


class GetTextTests(TestCase):
    def test_simple_pdf(self):
        test_file = os.path.join(TEST_FILES_DIR, 'simple.pdf')
        text = extractors.get_text(test_file)
        self.assertTrue(isinstance(text, basestring))
        # Checks if the result contains a word from the pdf
        self.assertRegexpMatches(text, 'Hello')

    def test_simple_pptx(self):
        test_file = os.path.join(TEST_FILES_DIR, 'simple.pptx')
        text = extractors.get_text(test_file)
        self.assertTrue(isinstance(text, basestring))
        # Checks if the result contains a word from the pdf
        self.assertRegexpMatches(text, 'Title')

    def test_long_pdf(self):
        """
            Tests if the extractor works in longer files
        """
        test_file = os.path.join(TEST_FILES_DIR, '5_0_customer.pdf')
        text = extractors.get_text(test_file)
        self.assertTrue(isinstance(text, basestring))
        # Checks if the result contains a word from the pdf
        self.assertRegexpMatches(text, 'HORLOGE')

    def test_long_pptx(self):
        test_file = os.path.join(TEST_FILES_DIR, 'constant_programming.pptx')
        text = extractors.get_text(test_file)
        self.assertTrue(isinstance(text, basestring))
        # Checks if the result contains a word from the pdf
        self.assertRegexpMatches(text, 'Programming')

    def test_unsupported_file(self):
        test_file = os.path.join(TEST_FILES_DIR, 'sample.txt')
        try:
            extractors.get_text(test_file)
            self.assertTrue(False)
        except FileExtensionNotSupportedException:
            pass
