from contextlib import closing
import logging
import os
import re

from cStringIO import StringIO

from ..exceptions import FileExtensionNotSupportedException


logger = logging.getLogger(__name__)


def extract_pdf_text(fname, pages=None):
    """
    Extracts text from a pdf file
    """
    try:
        from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
        from pdfminer.converter import TextConverter
        from pdfminer.layout import LAParams
        from pdfminer.pdfpage import PDFPage
    except ImportError:
        logger.error(
            "You need to install aditional dependencies for this to work. Try "
            "running `pip install liox_file_analyzer[documents]`"
        )
        raise

    logger.info(u'Starting extract_pdf_text: File: %s, Pages: %s', fname, pages)

    if not pages:
        page_nums = set()
    else:
        page_nums = set(pages)

    manager = PDFResourceManager()

    with closing(StringIO()) as output:
        with closing(TextConverter(manager, output, laparams=LAParams())) as converter:
            interpreter = PDFPageInterpreter(manager, converter)
            with file(fname, 'rb') as infile:
                for page in PDFPage.get_pages(infile, page_nums):
                    interpreter.process_page(page)
                text = output.getvalue()

    return text


def extract_pptx_text(fname, pages=None, include_notes=True):
    """
    Extracts text from a pptx file
    """

    def _get_text(paragraphs):
        """
        Loops through paragraph runs from pptx text_frame.paragraphs and
        concatenates the text.
        """
        text = ''
        for paragraph in paragraphs:
            for run in paragraph.runs:
                text = u'%s %s' % (text, run.text)
        return text

    try:
        from pptx import Presentation
    except ImportError:
        logger.error(
            "You need to install aditional dependencies for this to work. Try "
            "running `pip install liox_file_analyzer[documents]`"
        )
        raise

    logger.info(u'Starting extract_pptx_text: File: %s, Pages: %s', fname, pages)

    prs = Presentation(fname)

    text = u''
    for slide in prs.slides:
        for shape in slide.shapes:
            if shape.has_text_frame:
                # Get texts found on a Shape object in a slide.
                text = u'%s %s' % (text, _get_text(shape.text_frame.paragraphs))

            if shape.has_table:
                # Get texts from a Table inside the Shape object.
                for row_count, row in enumerate(shape.table.rows):
                    for column_count, column in enumerate(shape.table.columns):
                        # Get the paragraphs from the specific table cell.
                        text = u'%s %s' % (text, _get_text(shape.table.cell(row_count, column_count).text_frame.paragraphs))

        if include_notes:
            try:
                if slide.has_notes_slide:
                    for paragraph in slide.notes_slide.notes_text_frame.paragraphs:
                        text = u'%s %s' % (text, paragraph.text)
            except AttributeError:
                logger.warning(
                    'Extracting speaker notes not supported. Requires python-pptx version 0.6.2 or higher.'
                )

    return text


def clean_text(text):
    """
    Removes extra whitespace including tabs and newlines
    """
    return re.sub('\s+', ' ', text).strip()


def get_text(fname, **kwargs):
    """
    Extracts text from a file

    Keyword arguments:
        `pages`: int, number of pages to be extracted
            if None, all pages will be extracted
        `clean`: boolean, removes extra whitespace from the result
    """
    supported_filetypes = ('.pdf', '.pptx')
    ext = os.path.splitext(fname)[1]

    if ext not in supported_filetypes:
        raise FileExtensionNotSupportedException(
            'File not supported. %s files only' % str(supported_filetypes))

    pages = kwargs.get('pages', None)
    clean = kwargs.get('clean', False)

    if ext == '.pdf':
        text = extract_pdf_text(fname, pages)
    else:
        text = extract_pptx_text(fname, pages)

    if clean:
        return clean_text(text)
    return text
