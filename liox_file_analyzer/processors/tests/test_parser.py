import os
import logging
import zipfile

from collections import Counter
from unittest import TestCase

from ..utils import (
    is_doc_file,
    is_slides_file,
    is_video_file,
    is_archive_file,
    is_audio_file,
    extension_from_filename,
)
from ..processors import (
    process_doc,
    process_slides,
    process_video,
    process_archive,
    process_audio
)
from ..constants import SUPPORTED_VIDEO_FILE_TYPES

logging.basicConfig(level=logging.DEBUG)


class ParserTest(TestCase):

    test_files_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  'file_analysis')

    def test_process_doc(self):
        log = logging.getLogger('test_process_doc')
        for file_name in os.listdir(self.test_files_dir):
            if is_doc_file(extension_from_filename(file_name)):
                expected = int(file_name.split('_')[0])
                file_path = os.path.join(self.test_files_dir, file_name)
                log.debug("testing file: %s", file_path)

                proc_data = process_doc(file_path)
                log.debug("found %d words and %d pages", proc_data['words'], proc_data['pages'])
                log.debug("expected %d words got %d in %s",
                          expected, proc_data['words'], file_name)

                self.assertTrue(abs(proc_data['words']-expected)/expected < 0.15,
                                "expected %d words got %d in %s" % (expected,
                                                                    proc_data['words'],
                                                                    file_name))

    def test_process_slides(self):
        """
        Test processing of slide type files e.g. pdf, ppt, pptx
        """
        log = logging.getLogger('test_process_slides')
        for file_name in os.listdir(self.test_files_dir):
            if is_slides_file(extension_from_filename(file_name)):
                # Get expected pages and words from filename
                split_file_name = file_name.split('_')
                expected_pages = int(split_file_name[0])
                expected_words = int(split_file_name[1])

                file_path = os.path.join(self.test_files_dir, file_name)

                # Analyze slide type files
                log.debug("testing file: %s", file_path)
                proc_data = process_slides(file_path)

                # Information loggers
                log.debug("found %d words and %d pages",
                          proc_data['words'],
                          proc_data['pages'])

                # Assertions
                self.assertEquals(
                    proc_data['pages'],
                    expected_pages,
                    "expected %d pages got %d in %s" % (
                        expected_pages,
                        proc_data['pages'],
                        file_name
                    )
                )
                self.assertEquals(
                    proc_data['words'],
                    expected_words,
                    "expected %d words got %d in %s" % (
                        expected_words,
                        proc_data['words'],
                        file_name
                    )
                )

    def test_process_video(self):
        log = logging.getLogger('test_process_video')
        for file_name in os.listdir(self.test_files_dir):
            if is_video_file(extension_from_filename(file_name)):
                expected = int(file_name.split('_')[0])
                file_path = os.path.join(self.test_files_dir, file_name)
                log.debug("testing file: %s", file_path)
                proc_data = process_video(file_path)

                log.debug("expected %d minutes got %d in %s",
                          expected, proc_data['minutes'], file_name)

                self.assertEquals(proc_data['minutes'], expected,
                                  "expected %d minutes got %d in %s" % (expected,
                                                                        proc_data['minutes'],
                                                                        file_name))

    def test_process_audio(self):
        log = logging.getLogger('test_process_audio')
        for file_name in os.listdir(self.test_files_dir):
            if is_audio_file(extension_from_filename(file_name)):
                expected = int(file_name.split('_')[0])
                file_path = os.path.join(self.test_files_dir, file_name)
                log.debug("testing file: %s", file_path)
                proc_data = process_audio(file_path)
                log.debug("expected %d minutes got %d in %s",
                          expected, proc_data['minutes'], file_name)
                self.assertEquals(proc_data['minutes'], expected,
                                  "expected %d minutes got %d in %s" % (expected,
                                                                        proc_data['minutes'],
                                                                        file_name))

    def test_process_zip(self):
        log = logging.getLogger('test_process_zip')
        for file_name in os.listdir(self.test_files_dir):
            if is_archive_file(extension_from_filename(file_name)):
                expected = int(file_name.split('_')[0])
                file_path = os.path.join(self.test_files_dir, file_name)
                log.debug("testing file: %s", file_path)

                proc_data = process_archive(file_path)
                log.debug("found %d words and %d pages",
                          proc_data['words'], proc_data['pages'])
                log.debug("expected %d words got %d in %s",
                          expected, proc_data['words'], file_name)

                self.assertTrue(abs(proc_data['words']-expected)/expected < 0.15,
                                "expected %d seconds got %d in %s" % (expected,
                                                                      proc_data['words'],
                                                                      file_name))

    def test_process_story(self):
        """
        The story files on test_files_dir should have the expected units on the
        filename with the format: `<words>_<pages>_<minutes>_filename.story`
        """
        log = logging.getLogger('test_process_story')
        story_dir = os.path.join(self.test_files_dir, 'story_files')
        for file_name in os.listdir(story_dir):
            self.assertTrue(is_archive_file(extension_from_filename(file_name)))

            file_path = os.path.join(story_dir, file_name)
            log.debug("testing file: %s", file_path)

            proc_data = process_archive(file_path)

            expected_words = int(file_name.split('_')[0])
            expected_pages = int(file_name.split('_')[1])
            expected_minutes = int(file_name.split('_')[2])
            words = proc_data['words']
            pages = proc_data['pages']
            minutes = proc_data['minutes']

            log.debug('found %d words and %d pages and %s minutes',
                      words, pages, minutes)
            log.debug('expected %d words, %d pages and %s minutes',
                      words, pages, minutes)
            if expected_words:
                self.assertTrue(abs(proc_data['words']-expected_words)/expected_words < 0.15,
                                'expected %d words got %d in %s' % (expected_words, words, file_name))
            self.assertEquals(minutes, expected_minutes,
                              'expected %d minutes got %d in %s' % (expected_minutes,
                                                                    minutes,
                                                                    file_name))
            self.assertEquals(pages, expected_pages,
                              'expected %d pages got %d in %s' % (expected_pages,
                                                                    pages,
                                                                    file_name))

    def test_video_formats(self):
        """
        Tests the `video_formats.story` file.  This file contains 5 video files if
        opened as a .story file.  It has different file formats: FLV, MPG, AVI, MPEG, MP4
        but once it is extracted as an archive it will convert all into MPEG if it is not
        already MPEG and it leaves the original FLV file.
        """
        story_dir = os.path.join(self.test_files_dir, 'story_files')

        file_name = '0_0_6_video_formats.story'
        file_path = os.path.join(story_dir, file_name)
        zip_file = zipfile.ZipFile(file_path)

        video_files = []
        for _file in zip_file.namelist():
            extension = extension_from_filename(_file)
            if extension in SUPPORTED_VIDEO_FILE_TYPES:
                video_files.append(extension)

        count = Counter(video_files)
        self.assertEquals(len(video_files), 6)
        self.assertEquals(count['mpeg'], 5)
        self.assertEquals(count['flv'], 1)


    def test_process_odf(self):
        """
        Tests odf files words and pages units. The files on test_files_dir should have a
        filename with the format: `<words>_<pages>_filename.<ext>`
        """
        log = logging.getLogger('test_process_doc')
        _dir = os.path.join(self.test_files_dir, 'odf_files')

        for file_name in os.listdir(_dir):
            file_path = os.path.join(_dir, file_name)
            log.debug("testing file: %s", file_path)

            proc_data = process_doc(file_path)
            words = proc_data['words']
            pages = proc_data['pages']

            expected_words = int(file_name.split('_')[0])
            expected_pages = int(file_name.split('_')[1])
            self.assertTrue(abs(proc_data['words']-expected_words)/expected_words < 0.15,
                            'expected %d words got %d in %s' % (expected_words, words, file_name))
            self.assertEquals(pages, expected_pages,
                              'expected %d pages got %d in %s' % (expected_pages,
                                                                    pages,
                                                                    file_name))
