import re


POWERPOINT_FILE_TYPES = (
    'ppt', 'pptx',
)

ODF_EXTENSIONS = ['odf', 'odt', 'ods', 'ott']

SUPPORTED_TEXT_FILE_TYPES = [
    'doc', 'docx', 'xml', 'strings', 'po', 'pot', 'mif', 'inx',
    'xls',  'xlsx', 'txt', 'vtt', 'xliff', 'xlf', 'resjson',
    'html', 'rtf',  'htm', 'csv', 'srt', 'idml', 'json',
    'ini', 'properties', 'yaml', 'yml', 'resx', 'resw', 'psd',
    'erb', 'epub', 'sgml', 'tmx', 'flglo', 'fltoc', 'flsnp',
] + ODF_EXTENSIONS

SUPPORTED_SLIDE_FILE_TYPES = (
    'ppt', 'pdf', 'pptx'
)

SUPPORTED_VIDEO_FILE_TYPES = (
    'mp4', 'mov', 'flv', 'wmv', 'm4v', 'avi', 'mpeg'
)

SUPPORTED_IMAGE_FILE_TYPES = (
    'png', 'jpg', 'jpeg', 'bmp'
)

SUPPORTED_AUDIO_FILE_TYPES = (
    'mp3', 'wav', 'm4a', 'wma', 'ogg'
)

SUPPORTED_CONTENT_TYPES = {
    'application/json': ['json', 'resjson'],
    'application/pdf': ['pdf'],
    'application/epub+zip': ['epub'],
    'application/rtf': ['rtf'],
    'application/xml': ['idml', 'inx', 'resw', 'resx', 'xlf', 'xliff', 'xml'],
    'application/vnd.mif': ['mif'],
    'application/vnd.openxmlformats-officedocument.presentationml.presentation': ['pptx'],
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': ['xlsx'],
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document': ['doc', 'docx'],
    'application/vnd.oasis.opendocument.text': ODF_EXTENSIONS,
    'application/zip': ['zip'],
    'image/jpeg': ['jpg', 'jpeg'],
    'image/png': ['png'],
    'image/vnd.adobe.photoshop': ['psd'],
    'text/csv': ['csv'],
    'text/html': ['htm', 'html'],
    'text/plain': ['ini', 'po', 'pot', 'properties', 'srt', 'strings', 'txt', 'vtt'],
    'text/xml': ['xml'],
    'text/yaml': ['yml', 'yaml'],
    'video/mp4': ['mp4'],
    'video/quicktime': ['mov'],
    'video/x-flv': ['flv'],
    'video/x-m4v': ['m4v'],
    'video/x-ms-wmv': ['wmv'],
    'video/mpeg': ['mpeg']
}

SUPPORTED_ARCHIVE_FILE_TYPES = ('zip', 'story')

UNIT_TYPE_WORDS = 'words'
UNIT_TYPE_MINUTES = 'minutes'
UNIT_TYPE_PAGES = 'pages'
UNIT_TYPE_STANDARDIZED_PAGES = 'standardized_pages'
UNIT_TYPE_ROWS = 'rows'
UNIT_TYPE_CHARACTERS = 'characters'
UNIT_TYPE_FILES = 'files'

UNIT_TYPES = (UNIT_TYPE_WORDS,
              UNIT_TYPE_MINUTES,
              UNIT_TYPE_PAGES,
              UNIT_TYPE_STANDARDIZED_PAGES,
              UNIT_TYPE_ROWS,
              UNIT_TYPE_CHARACTERS,
              UNIT_TYPE_FILES)

SINGULARS = {
    UNIT_TYPE_WORDS: 'word',
    UNIT_TYPE_MINUTES: 'minute',
    UNIT_TYPE_PAGES: 'page',
    UNIT_TYPE_STANDARDIZED_PAGES: 'standardized page',
    UNIT_TYPE_ROWS: 'row',
    UNIT_TYPE_CHARACTERS: 'character',
    UNIT_TYPE_FILES: 'file',
}

PUNCTUATION = (
    '.', '...', '/', '?', '$', ':', ')', '(', ',',
    '[', ']', '\'', '"', '%', '*', "``", "\"\"", "\'\'",
    'gt', 'lt', 'CDATA', '&', '-', u'\u2013', u'\u2014'
)

VIDEO_VALIDATION_WINDOW_SECONDS = 5

EXCLUDE_DO_NOT_TRANSLATE_PATTERN = re.compile(r'\[(do-not-translate|dnt)\](((?!\[\/(do-not-translate|dnt)\]).)|\n)*\[\/(do-not-translate|dnt)\]')

WORDS_PER_MINUTE = 150
WORDS_PER_PAGE = 250
WORDS_PER_ROW = 250
WORDS_PER_SLIDE = 150

ASIAN_LANGUAGES = {
    'ja-jp': 3,
    'th-th': 6,
    'zh-cn': 1.4,
    'zh-tw': 1.4,
    'zh-hk': 1.4,
}
