import datetime
import io
import json
import logging
import math
import os
import re
import shutil
import urllib
import urllib2
import zipfile

from xml.dom.minidom import parse

from django.conf import settings
from django.utils.encoding import smart_str, smart_unicode
from django.core import exceptions

from nltk.tokenize import word_tokenize, sent_tokenize


from subprocess import Popen, PIPE
from time import time

from ..exceptions import FileExtensionNotSupportedException
from ..extractors import get_text
from .utils import (
    remove_markup_tags,
    extension_from_filename,
    media_type_from_extension,
    contains_digits,
    exclude_do_not_translate,
    exclude_no_translate_attributed_tags,
    empty_unit_type_dict,
    is_doc_file,
)
from .constants import (
    UNIT_TYPES,
    UNIT_TYPE_WORDS,
    UNIT_TYPE_MINUTES,
    UNIT_TYPE_PAGES,
    UNIT_TYPE_STANDARDIZED_PAGES,
    UNIT_TYPE_ROWS,
    UNIT_TYPE_CHARACTERS,
    UNIT_TYPE_FILES,
    PUNCTUATION,
    VIDEO_VALIDATION_WINDOW_SECONDS,
    SUPPORTED_IMAGE_FILE_TYPES,

    WORDS_PER_SLIDE,
    WORDS_PER_MINUTE,
    WORDS_PER_ROW,

    ASIAN_LANGUAGES,
    ODF_EXTENSIONS,
)
try:
    TMP_DIR = settings.TMP_DIR
except (exceptions.ImproperlyConfigured, AttributeError):
    TMP_DIR = '/tmp/'

logger = logging.getLogger(__name__)


def processor_from_media_type(media_type):
    """
    Returns a processor function based on a media type.
    All processors return a tuple of:
    * words
    * duration
    * pages
    """
    logger.debug('Start Processing Media Type: {}'.format(media_type))

    if media_type == 'video':
        processor = process_video
    elif media_type == 'audio':
        processor = process_audio
    elif media_type == 'doc':
        processor = process_doc
    elif media_type == 'zip':
        processor = process_archive
    elif media_type == 'archive':
        processor = process_archive
    elif media_type == 'slides':
        processor = process_slides
    elif media_type == 'image':
        processor = process_unitless
    else:
        processor = None

    return processor


def process_unitless(temp_file):
    logger.debug('Process Unitless')

    units = empty_unit_type_dict()

    # Overriding these specials
    units[UNIT_TYPE_PAGES] = 1
    units[UNIT_TYPE_STANDARDIZED_PAGES] = 1

    return units


def process_doc(temp_file, exclude_repetitions=False):
    logger.info('Process Doc')

    text, pages, rows = extract_text(temp_file)
    units = get_units_from_text(text, exclude_repetitions=exclude_repetitions)
    units[UNIT_TYPE_PAGES] = pages
    units[UNIT_TYPE_ROWS] = rows

    return units


def process_video(temp_file):
    try:
        from ffvideo import VideoStream
    except ImportError:
        logger.error(
            "You need to install aditional dependencies for this to work. Try "
            "running `pip install liox_file_analyzer[multimedia]`"
        )
        raise
    logger.debug('Process Video')
    validate_video(temp_file)
    vs = VideoStream(temp_file)

    units = empty_unit_type_dict()
    units[UNIT_TYPE_MINUTES] = int(math.ceil(vs.duration/60))

    return units


def process_audio(temp_file):
    try:
        import audioread
    except ImportError:
        logger.error(
            "You need to install aditional dependencies for this to work. Try "
            "running `pip install liox_file_analyzer[multimedia]`"
        )
        raise
    logger.debug('Process Audio on file %s' % temp_file)
    units = empty_unit_type_dict()
    try:
        with audioread.audio_open(temp_file) as f:
            logger.debug('Audio Duration = %d' % f.duration)
            units[UNIT_TYPE_MINUTES] = int(math.ceil(f.duration/60))
    except Exception as e:
        logger.error(e)

    return units


def process_slides(temp_file, exclude_repetitions=False):
    """
    This processor really just counts pages because
    counting words from these types of files is so unreliable.
    """
    logger.info('Process Slides')

    temp_file_extension = extension_from_filename(temp_file).lower()

    if temp_file_extension == 'pdf':
        try:
            from PyPDF2 import PdfFileReader
        except ImportError:
            logger.error(
                "You need to install aditional dependencies for this to work. "
                "Try running `pip install liox_file_analyzer[documents]`"
            )
            raise

        units = empty_unit_type_dict()

        with open(temp_file, 'rb') as f:
            pdf_file = PdfFileReader(f, strict=False)
            units[UNIT_TYPE_PAGES] = pdf_file.getNumPages()
            units[UNIT_TYPE_STANDARDIZED_PAGES] = units[UNIT_TYPE_PAGES]

    else:
        try:
            from pptx import Presentation
        except ImportError:
            logger.error(
                "You need to install aditional dependencies for this to work. "
                "Try running `pip install liox_file_analyzer[documents]`"
            )
            raise

        # try to analyze pptx file with pptx library
        try:
            text = get_text(temp_file)
            units = get_units_from_text(text, exclude_repetitions=exclude_repetitions)

            prs = Presentation(temp_file)

            pages_count = len(prs.slides)

            units[UNIT_TYPE_PAGES] = units[UNIT_TYPE_STANDARDIZED_PAGES] = pages_count

            if not units[UNIT_TYPE_WORDS]:
                if temp_file_extension == 'pptx' or temp_file_extension == 'ppt':
                    units[UNIT_TYPE_WORDS] = pages_count * WORDS_PER_SLIDE

        # pptx library can't analyze this file, analyze it as a slide
        except Exception as e:
            logger.debug('Error processing pptx file {}'.format(e))

    return units


def process_xml_story(temp_file):
    """
    XML files generated from inside a story archive contains meta data that should not be
    included in the translations. This is a custom xml processor for story files which extracts
    translatable texts from the different story file generator softwares namely "Storyist" &
    "Articulate Storyline", each have different xml attribute formats.
    """
    text = ''
    page = 0

    xml_dom = parse(temp_file)

    if xml_dom.documentElement.tagName == 'office:document':
        # Story files generated by "Storyist" are like pdf files.  The generated
        # XML files found in this type of story file use the <text> element which contains
        # the translatable texts.
        text = remove_markup_tags(' '.join([_dom.toxml() for _dom in \
            xml_dom.getElementsByTagNameNS('*', 'text')]))

    else:
        # XML generator used is "Articulate Storyline".
        # Story files generated by this software are like ppt files wherein the content
        # is placed in slides.  There are a lot of config XML for this type.  The translatable XML
        # texts are located on the `slides` directory with the <plain> element.  Additionally, slides
        # may have `notes` that contain translatable texts.
        if '/story/slides/' in temp_file:
            # Slides
            text = ' '.join([_dom.firstChild.nodeValue for _dom in \
                xml_dom.getElementsByTagName('plain') if _dom.firstChild])

            # Notes
            note_text = ' '.join([_dom.firstChild.nodeValue for _dom in \
                xml_dom.getElementsByTagName('note') if _dom.firstChild])
            if note_text:
                try:
                    from bs4 import BeautifulSoup
                except ImportError:
                    logger.error(
                        "You need to install aditional dependencies for this "
                        "to work. Try running `pip install "
                        "liox_file_analyzer[xml]`"
                    )
                    raise
                # Extract the note text from Span's Text attributes
                soup = BeautifulSoup(note_text, 'xml')
                text += ' '.join([content.get('Text', '').strip() for content in soup.find_all('Span')])
        else:
            # The other xml files only contain config attributes.
            return empty_unit_type_dict()

    if text:
        units = get_units_from_text(text)
        # Only count the file as 1 page if there is text content
        units[UNIT_TYPE_PAGES] = 1
        return units

    return empty_unit_type_dict()


def get_units_from_text(text, units=None, excluded_sentences=None, exclude_repetitions=False):
    if units is None:
        units = empty_unit_type_dict()
    if excluded_sentences is None:
        excluded_sentences = []

    words, characters, excerpt, \
        excluded_sentences, \
        distinct_sentences = count_words_chars(text, excluded_sentences, exclude_repetitions)

    units[UNIT_TYPE_WORDS] = words
    units[UNIT_TYPE_CHARACTERS] = characters
    units['excerpt'] = excerpt
    units['distinct_sentences'] = distinct_sentences

    return units


def count_words_chars(text, excluded_sentences=[], exclude_repetitions=False):
    """
    Accepts a string of ``text`` which is bleached and then tokenized, returning
    a tuple containing the number of words, characters, and an excerpt.
    The second argument ``excluded_sentences`` is a list which would be checked
    per sentence iteration if a sentence would be de-duplicated if the
    third argument ``exclude_repetitions`` == True
    """
    logger.debug(u'Counting Words and Characters')

    words = 0
    chars = 0
    excerpt = []
    distinct_sentences = {}

    # Strip out [do-not-translate]
    text = smart_unicode(text)
    text = exclude_do_not_translate(text)
    non_chars = re.compile(ur'[0-9\uff01-\uff20\uff3c-\uff40\uff5b-\uff65\u3000-\u303f\uff9e-\uff9f\uffe0-\uffee\u0020\u0021\u0022\u0023\u0025\u0026\u0027\u0028\u0029\u0040\u2010\u2011\u2012\u2013\u2014\u2015\u2018\u2019\u2039\u2309\u2329\u002a\u002c\u002d\u002e\u002f\u003a\u003b\u003f\u005b\u005c\u005d\u007b\u007d\u00a1\u00a7\u00ab\u00b6\u00bb\u201a\u201b\u201c\u201d\u201e\u201f\u203a\u230b\u232a\ufe58\ufe63]', re.UNICODE)

    for sentence in sent_tokenize(text):
        skip_sentence = False
        sentence_words = 0
        for word in word_tokenize(sentence):
            if len(excerpt) < 50 and not skip_sentence:
                excerpt.append(word)
            if word not in PUNCTUATION \
               and not word.isdigit() \
               and not contains_digits(word):
                if not skip_sentence:
                    words += 1
                    sentence_words += 1

        if not skip_sentence:
            chars += len(re.sub(non_chars, '', sentence))

            # Create key value pair to store to File.sentences
            # The Distict sentences in one file.
            distinct_sentences[sentence] = sentence_words

    excerpt = unicode(' '.join(excerpt))

    return words, chars, excerpt, excluded_sentences, distinct_sentences


def process_archive(temp_file):
    """
    Extracts files from a zip and counts words, characters, duration, and pages
    """
    units = empty_unit_type_dict()
    units['excerpt'] = ''
    units['distinct_sentences'] = {}

    logger.debug('opening zip')
    zip_file = zipfile.ZipFile(temp_file)

    timed_at = str(time()).replace('.', '_')
    out_dir = os.path.join(TMP_DIR, timed_at)
    story_archive = extension_from_filename(temp_file).lower() == 'story'

    for one_file in zip_file.namelist():

        logger.debug('extracting file %s to %s' % (one_file, out_dir))

        extracted_file = zip_file.extract(one_file, out_dir)
        extension = extension_from_filename(one_file)

        if not extension:
            # Directory inside the archive file, do not run a processor
            continue

        if story_archive:
            proc_data = process_story(extracted_file)
        else:
            processor = processor_from_media_type(media_type_from_extension(extension))
            proc_data = {}
            if processor:
                proc_data = processor(extracted_file)

        for key in UNIT_TYPES:
            units[key] += proc_data.get(key, 0)
        units['excerpt'] += proc_data.get('excerpt', '')
        units['distinct_sentences'].update(proc_data.get('distinct_sentences', {}))

    zip_file.close()

    shutil.rmtree(out_dir)

    logger.debug('Trigger Remove Zip File')

    return units


def process_story(extracted_file):
    extension = extension_from_filename(extracted_file)
    proc_data = {}

    if extension == 'xml':
        proc_data = process_xml_story(extracted_file)
    elif extension == 'mpeg':
        proc_data = process_video(extracted_file)
    elif extension in SUPPORTED_IMAGE_FILE_TYPES:
        # Do not count image files as pages/standardized_pages like @process_unitless
        pass
    else:
        processor = processor_from_media_type(media_type_from_extension(extension))
        if processor:
            proc_data = processor(extracted_file)

    return proc_data


def get_string_values(obj):
    """Recursive function to pull strings out of a list or nested dictionary."""

    if isinstance(obj, basestring):
        return obj
    elif isinstance(obj, list):
        return ' '.join([get_string_values(item) for item in obj])
    elif isinstance(obj, dict):
        return ' '.join([get_string_values(value) for value in obj.values()])
    else:
        return str(obj)


def get_layer_text(obj):
    """
    Recursive function to go through layers of a PDF
    """
    # Installation of `psd_tools` has already been taken care of by try/catch
    # statement earlier
    from psd_tools.user_api.psd_image import Group, Layer

    text = ''
    if isinstance(obj, Layer) and obj.text_data:
        text = obj.text_data.text
    elif isinstance(obj, Group):
        for item in obj.layers:
            text = text + ' ' + get_layer_text(item)
    return text


def get_file_text(file_path):
    """
    We have been running into text files with different encodings.
    This tries utf-8, utf-16, ascii
    """

    supported_codecs = ('ascii', 'utf-8', 'utf-16', 'iso-8859-1')

    for codec in supported_codecs:
        try:
            with io.open(file_path, 'r', encoding=codec) as f:
                file_content = f.read()
            return file_content
        except UnicodeDecodeError:
            logger.debug('Decoding error: {} - {}'.format(codec, file_path))
            continue
        except UnicodeError:
            logger.debug('Unicode error: {} - {}'.format(codec, file_path))
            continue

    return ''


def extract_text(file_path):
    """
    Extracts text
    """
    file_extension = extension_from_filename(file_path)

    logger.debug(u'Extract Text. Extension: %s', file_extension)

    pages = 0
    rows = 0
    text = ''

    if file_extension in ("txt", "csv"):
        text = get_file_text(file_path)
        # remove null values
        text = text.strip('\0')
        # removing the header row
        rows = max(len(text.splitlines()) - 1, 0)

    elif file_extension in ('properties', 'ini'):

        pattern = re.compile('^.*=(.*)$')

        with open(file_path, 'r') as f:
            for line in f.readlines():
                match = re.search(pattern, line)
                if match and line[0] != ';':
                    # skipping punctuation lines
                    text += ' ' + ' '.join(match.groups())

    elif file_extension == 'strings':

        file_content = get_file_text(file_path)

        # stripping out comments
        text = re.sub('/\*.*\*/', '', file_content)

        # get values of key value pairs
        text = '. '.join(re.findall('=\s?\"(.*)\";', text))

        # exclude placeholders
        text = re.sub(r'(%[^%\s,.();:\"\'!?]+)', '', text)

    elif file_extension in ('json', 'resjson'):
        with open(file_path, 'r') as f:
            file_content = f.read()
        json_data = json.loads(file_content)
        text = get_string_values(json_data)

    elif file_extension in ['po', 'pot']:
        """
        Decoding po files using python polib.  Loop through supported_codecs.
        Set last codec to None to enable detect_encoding from polib.
        """
        try:
            import polib
        except ImportError:
            logger.error(
                "You need to install aditional dependencies for this to work. "
                "Try running `pip install liox_file_analyzer[software]`"
            )
            raise

        supported_codecs = ('utf-8', 'ascii', 'utf-16', 'iso-8859-1', None)

        for codec in supported_codecs:
            try:
                p = polib.pofile(file_path, encoding=codec)
                text = '. '.join([e.msgid for e in p.translated_entries()])
                text += '. '.join([e.msgid for e in p.untranslated_entries()])
                break
            except UnicodeDecodeError as e:
                msg = 'Decoding Error: {}. File: {}. String Error: {}'.format(
                    e.encoding, file_path, e.object)
                logger.debug(msg)
                raise e
            except UnicodeError:
                msg = 'Unicode Error: {}. File: {}. String Error: {}'.format(
                    e.encoding, file_path, e.object)
                logger.debug(msg)
                raise e

    elif file_extension in ('yaml', 'yml'):
        try:
            import yaml
        except ImportError:
            logger.error(
                "You need to install aditional dependencies for this to work. "
                "Try running `pip install liox_file_analyzer[software]`"
            )
            raise

        file_content = get_file_text(file_path)
        try:

            text += get_string_values(yaml.load(file_content))
        except (yaml.composer.ComposerError,
                yaml.scanner.ScannerError,
                TypeError):

            with open(file_path, 'r') as f:
                for line in f.readlines():
                    try:
                        text += ' ' + line.split(':', 1)[1] + '.'
                    except (AttributeError, IndexError):
                        pass

        # stripping out variables, anchors, and aliases
        text = re.sub('%\{\w+\}', ' ', text)
        text = re.sub('&\w+', ' ', text)
        text = re.sub('\*\w+', ' ', text)

    elif file_extension in ('resx', 'resw'):
        try:
            from bs4 import BeautifulSoup
        except ImportError:
            logger.error(
                "You need to install aditional dependencies for this to work. "
                "Try running `pip install liox_file_analyzer[xml]`"
            )
            raise
        pages = 1
        with open(file_path, 'r') as file_obj:
            file_content = file_obj.read()
        soup = BeautifulSoup(file_content)

        for item in soup.find_all('data'):
            if item.has_attr('xml:space') and 'preserve' in item.attrs['xml:space']:
                text += '. ' + ' '.join([' '.join(value.stripped_strings) for value in item.find_all('value')])

    elif file_extension == 'srt':
        # Stripping out the time stamps and condensing
        with open(file_path, 'r') as f:
            file_content = f.read()
        text = re.sub('\d+.*\n\d\d:\d\d:\d\d,\d\d\d --> \d\d:\d\d:\d\d,\d\d\d', ' ', file_content)

    elif file_extension == 'vtt':
        # Stripping out the time stamps and condensing

        with open(file_path, 'r') as f:
            file_content = f.read()
        text = re.sub('\d\d:\d\d:\d\d.\d\d\d --> \d\d:\d\d:\d\d.\d\d\d', ' ', file_content)

    elif file_extension == "docx":
        try:
            from docx import opendocx, getdocumenttext
        except ImportError:
            logger.error(
                "You need to install aditional dependencies for this to work. "
                "Try running `pip install liox_file_analyzer[documents]`"
            )
            raise

        with open(file_path, 'r') as file_obj:
            try:
                # Process .docx filetype
                document = opendocx(file_obj)
                document_text_list = getdocumenttext(document)
                document_text = [smart_str(p) for p in document_text_list]
                text = " ".join(document_text)

            except (zipfile.BadZipfile, KeyError) as e:
                # It raises BadZipfile when the format is actually .doc but the file
                # extension is .docx
                logger.debug('Error processing docx file, format is not valid. {}'.format(e))
                raise Exception('Error processing docx file, format is not valid')

    elif file_extension in ("doc",):
        # Using Catdoc made more sense. No python library could interpret
        # Doc files better.
        with open(file_path, 'r') as file_obj:
            p = Popen(["catdoc", "w"], stdin=file_obj, stdout=PIPE, shell=True)
            output = p.communicate()[0]

        text = output

    elif file_extension in ODF_EXTENSIONS:

        text, pages = extract_odf_text(file_path)

    elif file_extension == "rtf":
        try:
            from pyth.plugins.rtf15.reader import Rtf15Reader
            from pyth.plugins.plaintext.writer import PlaintextWriter
        except ImportError:
            logger.error(
                "You need to install aditional dependencies for this to work. "
                "Try running `pip install liox_file_analyzer[documents]`"
            )
            raise

        with open(file_path, 'r') as file_obj:
            doc = Rtf15Reader.read(file_obj)

        text = PlaintextWriter.write(doc).getvalue()

    elif file_extension == 'idml':

        try:
            import ucf
            from bs4 import BeautifulSoup
        except ImportError:
            logger.error(
                "You need to install aditional dependencies for this to work. "
                "Try running `pip install liox_file_analyzer[xml]`"
            )
            raise

        my_doc = ucf.UCF(filename=file_path)
        for key in my_doc.keys():
            if key.startswith('Stories/'):
                soup = BeautifulSoup(my_doc[key])
                for story in soup.find_all('story'):
                    text += '. ' + ' '.join([' '.join(content.stripped_strings) for content in story.find_all('content')])

    elif file_extension == 'mif':
        with open(file_path, 'r') as f:
            file_content = f.read()
        strings = re.findall('<ParaLine\s+<String `(.*)\'>', file_content)
        text = '. '.join(strings)

    elif file_extension == 'inx':
        try:
            from bs4 import BeautifulSoup
        except ImportError:
            logger.error(
                "You need to install aditional dependencies for this to work. "
                "Try running `pip install liox_file_analyzer[xml]`"
            )
            raise
        pages = 1
        with open(file_path, 'r') as file_obj:
            file_content = file_obj.read()
        soup = BeautifulSoup(file_content)
        text = ' '.join([' '.join(contents.stripped_strings) for contents in soup.find_all('txsr')])
        # Strip out c_ because i think these are application codes.
        text = re.sub('c_\w+', '', text)
        text = re.sub('c_\s', ' ', text)

    elif file_extension in ('xliff', 'xlf'):
        try:
            from bs4 import BeautifulSoup
        except ImportError:
            logger.error(
                "You need to install aditional dependencies for this to work. "
                "Try running `pip install liox_file_analyzer[xml]`"
            )
            raise
        pages = 1
        text = ''

        with open(file_path, 'r') as file_obj:
            file_content = file_obj.read()
        soup = BeautifulSoup(file_content, 'xml')

        # Iterate through trans-unit tags
        for trans_unit in soup.find_all('trans-unit'):
            # Check trans-unit tags for translate attribute
            if trans_unit.get('translate', 'yes') == 'no':
                continue
            # Retrieve text from each <source>
            for contents in trans_unit.find_all('source'):
                # Parse the contents inside `source` to remove html/xml tags
                text = u". ".join((text, BeautifulSoup(contents.get_text(), 'lxml').get_text()))

    elif file_extension in ('xml', 'sgml', 'flglo',):
        try:
            from bs4 import BeautifulSoup
        except ImportError:
            logger.error(
                "You need to install aditional dependencies for this to work. "
                "Try running `pip install liox_file_analyzer[xml]`"
            )
            raise

        pages = 1

        with open(file_path, 'r') as file_obj:
            file_content = file_obj.read()
        soup = BeautifulSoup(file_content, 'xml')

        # remove tags attributed translate="no"
        if file_extension == 'xml':
            soup = exclude_no_translate_attributed_tags(soup)

        text = soup.get_text()

    elif file_extension in ('html', 'htm', 'flsnp',):
        try:
            from bs4 import BeautifulSoup
        except ImportError:
            logger.error(
                "You need to install aditional dependencies for this to work. "
                "Try running `pip install liox_file_analyzer[xml]`"
            )
            raise

        with open(file_path, 'r') as file_obj:
            file_content = file_obj.read()
        soup = BeautifulSoup(file_content)

        # remove tags attributed translate="no"
        soup = exclude_no_translate_attributed_tags(soup)

        text = ''

        for alt in soup.find_all(alt=True):
            text += ". " + alt['alt']

        for title in soup.find_all(title=True):
            text += ". " + title['title']

        for title_tag in soup(["title"]):
            text += ". " + title_tag.get_text()

        # remove invisible parts
        for script in soup(["script", "style", "head"]):
            script.decompose()

        text += '. ' + soup.get_text(". ")

    elif file_extension == "fltoc":
        try:
            from bs4 import BeautifulSoup
        except ImportError:
            logger.error(
                "You need to install aditional dependencies for this to work. "
                "Try running `pip install liox_file_analyzer[xml]`"
            )
            raise

        with open(file_path, 'r') as file_obj:
            file_content = file_obj.read()

        # Each TocEntry element corresponds to a topic file. The relative file location is
        # indicated by the Link attribute and the Title attribute takes the value of the
        # title element in the topic.
        soup = BeautifulSoup(file_content, 'xml')
        text += ' '.join([content.get('Title', '').strip() for content in \
                    soup.find_all('TocEntry')])

    elif file_extension == "erb":
        try:
            from bs4 import BeautifulSoup
        except ImportError:
            logger.error(
                "You need to install aditional dependencies for this to work. "
                "Try running `pip install liox_file_analyzer[xml]`"
            )
            raise

        pages = 1

        with open(file_path, 'r') as file_obj:
            file_content = file_obj.read()
        text = BeautifulSoup(file_content).get_text()

        # Pulling out ERB Variables and code
        text = re.sub('{.*}', ' ', text)
        text = re.sub('<%=.*%>', ' ', text)

    elif file_extension in ("xls", "xlsx"):
        try:
            import xlrd
        except ImportError:
            logger.error(
                "You need to install aditional dependencies for this to work. "
                "Try running `pip install liox_file_analyzer[documents]`"
            )
            raise

        with open(file_path, 'r') as file_obj:
            file_content = file_obj.read()
        wb = xlrd.open_workbook(file_contents=file_content)

        for sheet in wb.sheets():
            pages += 1
            for i in range(sheet.nrows):
                try:
                    row_text = ' ' + (". ".join(smart_str(cell)
                        for cell in sheet.row_values(i))) + ".\n"
                    # only counting the row if there is some data in it.
                    if len(row_text.strip()) > 0:
                        rows += 1
                    text += row_text
                except Exception as e:
                    logger.debug('Error processing excel cells {}'.format(e))

        # removing the header row
        rows -= 1

    elif file_extension == "psd":
        try:
            from psd_tools import PSDImage
        except ImportError:
            logger.error(
                "You need to install aditional dependencies for this to work. "
                "Try running `pip install liox_file_analyzer[documents]`"
            )
            raise

        psd = PSDImage.load(file_path)

        pages = 1

        text = ''

        for group in psd.layers:
            text = text + ' ' + get_layer_text(group)

    elif file_extension == 'epub':
        try:
            import epub
        except ImportError:
            logger.error(
                "You need to install aditional dependencies for this to work. "
                "Try running `pip install liox_file_analyzer[documents]`"
            )
            raise

        book = epub.open_epub(file_path, 'r')

        # get titles in metadata
        metadata = book.opf.metadata
        if metadata.titles:
            text += ' '.join(metadata.titles[0])

        # get xhtml and table of contents file
        for item in book.opf.manifest.values():
            if item.media_type == 'application/xhtml+xml' or \
                    item.media_type == 'application/x-dtbncx+xml':
                try:
                    from bs4 import BeautifulSoup
                except ImportError:
                    logger.error(
                        "You need to install aditional dependencies for this "
                        "to work. Try running `pip install "
                        "liox_file_analyzer[xml]`"
                    )
                    raise

                item_text = book.read_item(item)
                soup = BeautifulSoup(item_text)

                # remove invisible parts
                for script in soup(["script", "style", "head"]):
                    script.decompose()

                # read the nav map only for table of contents file
                if item.media_type == 'application/x-dtbncx+xml':
                    for script in soup(["navlabel"]):
                        text += ' ' + script.extract().get_text()
                else:
                    text += ' ' + soup.get_text()

                pages += 1
    elif file_extension in ('tmx'):
        try:
            from bs4 import BeautifulSoup
        except ImportError:
            logger.error(
                "You need to install aditional dependencies for this to work. "
                "Try running `pip install liox_file_analyzer[xml]`"
            )
            raise

        pages = 1
        text = ''

        file_content = get_file_text(file_path)

        soup = BeautifulSoup(file_content)
        # Get source language which by default is located on header
        header = soup.find('header')
        source_language = header.get('srclang')

        tu = soup.find('tu')
        source_language = tu.get('srclang', source_language)

        for tuv in soup.find_all('tuv', {'xml:lang': source_language}):
            # build text from source segments, delimited by '.'
            text += ' '.join(['. ' + ' '.join(contents.stripped_strings) for contents in tuv.find_all('seg')])
    else:

        logger.debug('File {}'.format(file_path))
        raise FileExtensionNotSupportedException(
            "%s file extension not supported." % file_extension)

    extensions_with_markup = [
        'properties', 'ini', 'strings', 'json', 'resjson', 'po', 'pot',
        'yaml', 'yml', 'resx', 'resw', 'srt', 'vtt', 'idml', 'mif', 'inx',
        'xliff', 'xlf', 'xml', 'sgml', 'html', 'html', 'erb', 'epub',
    ]

    if file_extension in extensions_with_markup:
        text = remove_markup_tags(text)

    return (text, pages, rows)

def extract_odf_text(file_path):
    """
    An OpenDocument file is like a zip archive but with a different extension.
    The files in the zip contain XML files with the text content stored on
    'content.xml' and 'styles.xml' in `<text:p/>` elements.  The `meta.xml` holds the
    file information like character, page and all word count.
    This method returns text and pages.
    """
    try:
        from bs4 import BeautifulSoup
    except ImportError:
        logger.error(
            "You need to install aditional dependencies for this to work. "
            "Try running `pip install liox_file_analyzer[xml]`"
        )
        raise

    text = ''
    pages = 1

    odf_file = zipfile.ZipFile(file_path)

    for odf_xml in ('content.xml', 'styles.xml', 'meta.xml'):
        xml_content = odf_file.read(odf_xml)
        soup = BeautifulSoup(xml_content, 'xml')

        if odf_xml == 'meta.xml':
            pages = int(soup.find('document-statistic').attrs.get('meta:page-count', 1))
        else:
            text += soup.get_text(separator=u' ')

    odf_file.close()

    return text, pages

def detect_file_language(excerpt):
    logger.debug('Detect Language')

    t1 = datetime.datetime.now()

    excerpt = excerpt[:900]

    # detect language via Google API
    params = urllib.urlencode({'key': settings.OD_GOOGLE_TRANSLATE_API_KEY, 'q': smart_str(excerpt)})
    response = urllib2.urlopen('https://www.googleapis.com/language/translate/v2/detect?' + params)
    data = json.load(response)
    lang = data['data']['detections'][0][0]['language']

    t2 = datetime.datetime.now()
    logger.debug('Done Detect Language Time: {}'.format((t2 - t1)))

    return lang


def characters_per_page(locale):
    if locale == 'zh':  # Chinese
        return 833
    elif locale == 'ko':  # Korean
        return 454.5
    elif locale == 'ja':  # Japanese
        return 500
    elif locale == 'th':  # Thai
        return 250
    else:  # Latin
        return 1500


def validate_video(path):
    """
    Checks if the video is not broken or incomplete.
    """
    # Installation of `ffvideo` has already been taken care of by try/catch
    # statement earlier
    from ffvideo import VideoStream
    vs = VideoStream(path)

    # Iterate through frames
    try:
        frame = None
        framecount = 0
        for frame in vs:
            framecount += 1
        last_timestamp = frame.timestamp
    except Exception as e:
        logger.info(e)
        raise Exception("Cannot read through frames.")

    # Check if the timestamp of the last frame is in between the duration with
    # a n second window.
    if vs.duration - VIDEO_VALIDATION_WINDOW_SECONDS <= last_timestamp <= vs.duration + VIDEO_VALIDATION_WINDOW_SECONDS:
        pass
    else:
        raise Exception("Video duration is not close to its last frame timestamp.")

    # Check if number of frames is equal to duration * framerate
    # but add a n frame window
    frame_window = vs.framerate * VIDEO_VALIDATION_WINDOW_SECONDS
    correct_framecount = vs.framerate*vs.duration
    if correct_framecount-frame_window <= framecount <= correct_framecount+frame_window:
        pass
    else:
        raise Exception("Video frame count is not close to its framerate multiplied by its duration.")

    return


def count_asian_words(language_code, characters):
    """ Returns number of words for asian language """
    # If language is Asian (japanese, chinese, thai, korean)
    return int(math.ceil(characters/ASIAN_LANGUAGES[language_code]))


def get_file_units(file_obj, unit_type, language_code):
    """
    Gets the number of units used to price the file.
    This depends on the service's unit type.
    """
    if unit_type == UNIT_TYPE_FILES:
        return 1
    elif unit_type == UNIT_TYPE_WORDS and not file_obj.words:
        if is_doc_file(file_obj.extension):
            return 0
        elif file_obj.pages:
            return file_obj.pages * WORDS_PER_SLIDE
        elif file_obj.minutes:
            return file_obj.minutes * WORDS_PER_MINUTE
        elif file_obj.rows:
            return file_obj.rows * WORDS_PER_ROW
        else:
            return 0
    elif unit_type == UNIT_TYPE_WORDS and language_code in ASIAN_LANGUAGES and file_obj.characters:
        return count_asian_words(language_code, file_obj.characters)
    else:
        return getattr(file_obj, unit_type.lower())
