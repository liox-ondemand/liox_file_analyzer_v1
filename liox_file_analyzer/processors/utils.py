# -*- coding: utf-8 -*-
import re
import bleach

from bs4 import BeautifulSoup
from xml.sax.saxutils import unescape

from constants import (
    UNIT_TYPES,
    POWERPOINT_FILE_TYPES,
    SUPPORTED_VIDEO_FILE_TYPES,
    SUPPORTED_TEXT_FILE_TYPES,
    SUPPORTED_AUDIO_FILE_TYPES,
    SUPPORTED_SLIDE_FILE_TYPES,
    SUPPORTED_IMAGE_FILE_TYPES,
    SUPPORTED_ARCHIVE_FILE_TYPES,
    SUPPORTED_CONTENT_TYPES,
    EXCLUDE_DO_NOT_TRANSLATE_PATTERN,
)
_digits_pattern = re.compile('\d')


def empty_unit_type_dict():
    """
    Simple litte method to create a unit type dictionary out of
    all of the possible unit types.
    """
    unit_type_dict = {}

    for unit_type in UNIT_TYPES:
        unit_type_dict[unit_type] = 0

    unit_type_dict['excerpt'] = ''

    return unit_type_dict


def remove_markup_tags(text):
    """
    Removes tags from text including escaped tags i.e. "&lt;li&gt;" (<li>)

    Note: This will replace escaped characters i.e.:
        &quot; = "
        &amp; = &
        &apos; = '

    >>> remove_markup_tags('&lt;li&gt;')
    u''
    >>> remove_markup_tags('<li>text</li>')
    u'text'
    >>> remove_markup_tags('<h1>text</h1>')
    u'text'
    >>> remove_markup_tags('<h1>text')
    u'text'
    >>> remove_markup_tags('&quot;')
    u'"'
    >>> remove_markup_tags('&amp;')
    u'&'
    >>> remove_markup_tags('&apos;')
    u"'"
    >>> remove_markup_tags(None)
    >>>
    """
    try:
        # We need to add unescape after bleach.clean because escaped characters will not be replaced
        # i.e. &amp; will remain &amp; without unescape after bleach.clean
        # >>> bleach.clean(unescape('&amp;'), tags=[], strip=True)
        # u'&amp;'
        if not isinstance(text, unicode):
            # Check if the text is not unicode, decode it first with the ``ignore`` option because
            # ``bleach.clean`` executes ``force_unicode`` on the text but for characters that could
            # not be decoded it raises UnicodeDecodeError.  Using ``ignore`` will remove the character.
            text = text.decode('utf-8', errors='ignore')
        return unescape(bleach.clean(unescape(text), tags=[], strip=True))
    except AttributeError:
        # This could be that text=None
        return text


def extension_from_filename(filename):
    """
    Returns a lower case of the extension based on a filename

    >>> extension_from_filename('foo')
    ''
    >>> extension_from_filename('foo.')
    ''
    >>> extension_from_filename('.foo')
    'foo'
    >>> extension_from_filename('.FOO')
    'foo'
    >>> extension_from_filename('foo.bar')
    'bar'
    >>> extension_from_filename('foo.bar.baz')
    'baz'
    >>> extension_from_filename('.foo.bar')
    'bar'
    """
    if '.' in filename:
        return filename.rsplit('.', 1)[1].lower()
    else:
        return ''


def is_powerpoint_file(filename):
    """
    Determines if a file is a powerpoint file

    >>> is_powerpoint_file('not_a_powerpoint.doc')
    False
    >>> is_powerpoint_file('powerpoint_file.pptx')
    True
    >>> is_powerpoint_file('powerpoint_file.ppt')
    True
    """
    return extension_from_filename(filename) in POWERPOINT_FILE_TYPES


def contains_digits(text):
    """
    Returns a boolean if text contains digits

    >>> contains_digits("Hello1")
    True
    >>> contains_digits("Hello")
    False
    """
    return bool(_digits_pattern.search(text))


def decode_media_type(media_type, file_extension):
    """
    Takes a media type like 'text/plain' and returns a file type that we use
    for parsing the file, like 'doc'.
    """
    if 'video' in media_type:
        return 'video'
    elif any(ext in media_type for ext in ['presentation', 'ms-powerpoint', 'pdf']):
        # .pot is an extension for both PowerPoint files and text translation
        # files. We do not support the former. But if presentation software is
        # installed on the user's computer and they upload a .pot translation
        # file it may be sent with a media type like 'application/ms-powerpoint'.
        # So we have a special case here to be sure that .pot files are treated
        # as doc and not slides.
        if file_extension in ['pot']:
            return 'doc'
        return 'slides'
    elif 'image' in media_type:
        return 'image'
    elif 'zip' in media_type:
        return 'zip'
    elif 'audio' in media_type:
        return 'audio'
    else:
        return 'doc'


def media_type_from_extension(extension):
    '''
    Returns a media type based on an extension.
    Uses supported file types constants to make the determination
    '''
    if extension in SUPPORTED_VIDEO_FILE_TYPES:
        return 'video'
    elif extension in SUPPORTED_TEXT_FILE_TYPES:
        return 'doc'
    elif extension in SUPPORTED_AUDIO_FILE_TYPES:
        return 'audio'
    elif extension in SUPPORTED_SLIDE_FILE_TYPES:
        return 'slides'
    elif extension in SUPPORTED_IMAGE_FILE_TYPES:
        return 'image'
    elif extension in SUPPORTED_ARCHIVE_FILE_TYPES:
        return 'archive'
    else:
        return ''


def extension_supported(extension):
    """
    Returns a boolean and checks if an extension is supported

    >>> extension_supported('txt')
    True
    >>> extension_supported('non-txt')
    False
    """
    if not media_type_from_extension(extension):
        return False
    return True


def is_video_file(extension):
    """
    Returns a boolean and checks if the extension is a video file

    >>> is_video_file('mp4')
    True
    >>> is_video_file('txt')
    False
    """
    return 'video' == media_type_from_extension(extension)


def is_audio_file(extension):
    """
    Returns a boolean that checks if the extension is an audio file

    >>> is_audio_file('wav')
    True
    >>> is_audio_file('mov')
    False
    """
    return 'audio' == media_type_from_extension(extension)


def is_doc_file(extension):
    """
    Returns a boolean that checks if the extension a document file

    >>> is_doc_file('txt')
    True
    >>> is_doc_file('jpg')
    False
    """
    return 'doc' == media_type_from_extension(extension)


def is_archive_file(extension):
    """
    Returns a boolean whether the extension is a zip

    >>> is_archive_file('zip')
    True
    >>> is_archive_file('txt')
    False
    """
    return 'archive' == media_type_from_extension(extension)


def is_slides_file(extension):
    """
    Returns a boolean whether the extension is a ppt/pptx/pdf

    >>> is_slides_file('pdf')
    True
    >>> is_slides_file('txt')
    False
    """
    return 'slides' == media_type_from_extension(extension)


def is_image_file(extension):
    """
    Returns a boolean whether the extension is an image file

    >>> is_image_file('png')
    True
    >>> is_image_file('mov')
    False
    """
    return 'image' == media_type_from_extension(extension)


def content_type_from_extension(extension):
    """
    Returns the content type based from extension

    >>> content_type_from_extension('txt')
    'text/plain'
    >>> content_type_from_extension('json')
    'application/json'
    """
    for ct, ext_list in SUPPORTED_CONTENT_TYPES.items():
        if extension in ext_list:
            return ct
    return None


def exclude_do_not_translate(text):
    """
    Returns the text without the 'do-not-translate' and 'dnt' tags

    >>> words = 'Text in [do-not-translate]Do Not Translate[/do-not-translate] and [dnt]the'\
                'newer "dnt" tags[/dnt] are excluded from the word count.'
    >>> words = exclude_do_not_translate(words)
    >>> print words.split()
    ['Text', 'in', 'and', 'are', 'excluded', 'from', 'the', 'word', 'count.']
    >>> bool(len(words.split()) == 9)
    True
    """
    p = EXCLUDE_DO_NOT_TRANSLATE_PATTERN
    p = p.sub('', text)
    return p

def exclude_no_translate_attributed_tags(soup):
    """
    Expects and returns a BeautifulSoup instance with tags
    attributed with translate='no' are removed.
    """
    for tag in soup.find_all(translate='no'):
        tag.extract()
    return soup
